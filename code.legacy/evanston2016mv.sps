                           	*SPSS Regression.
	   		             *EVANSTON & NEW TRIER 2016.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt17andregt23jan4.sav'.
*select if (amount1>140000).
*select if (amount1<9700000).
*select if (multi<1).
*select if sqftb<12000.

compute bsf=sqftb.
compute lsf=sqftl.


Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If (yr=15 and amount1>0) year1=2015.
If (yr=14 and amount1>0) year1=2014.
If (yr=13 and amount1>0)  year1=2013. 
If  (yr=12 and amount1>0) year1=2012.
If  (yr=11 and amount1>0) year1=2011.
If  (yr=10 and amount1>0) year1=2010. 
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004. 
exe.
*select if (year1>2010).
set mxcells=2000500.

Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	5333030360000
or pin=	5333101250000
or pin=	10102000430000
or pin=	10102000610000
or pin=	10111000390000
or pin=	10111000750000
or pin=	10111000790000
or pin=	10113000010000
or pin=	10113000330000
or pin=	10113000490000
or pin=	10113020340000
or pin=	10113030060000
or pin=	10113030400000
or pin=	10114130120000
or pin=	10114130130000
or pin=	10121030090000
or pin=	10122000220000
or pin=	10122000330000
or pin=	10122050210000
or pin=	10123060280000
or pin=	10123080130000
or pin=	10123080140000
or pin=	10123090040000
or pin=	10123110230000
or pin=	10123180130000
or pin=	10123210040000
or pin=	10123210330000
or pin=	10124030030000
or pin=	10124090270000
or pin=	10124110140000
or pin=	10131000120000
or pin=	10142010070000
or pin=	10112040140000
or pin=	10113230170000
or pin=	10114060050000
or pin=	10114100280000
or pin=	10124250080000
or pin=	11071080200000
or pin=	11071090140000
or pin=	11071120200000
or pin=	11071170340000
or pin=	11071190410000
or pin=	11181010230000
or pin=	11181020100000
or pin=	11181030110000
or pin=	11181080170000
or pin=	11181080290000
or pin=	11181130300000
or pin=	11181260100000
or pin=	5353170280000
or pin=	5354050140000
or pin=	5354090040000
or pin=	11071100450000
or pin=	11071160310000
or pin=	11181100330000
or pin=	11184200180000
or pin=	11192020080000
or pin=	11192160130000
or pin=	5354000280000
or pin=	5354000290000
or pin=	5354000360000
or pin=	5354010220000
or pin=	11192060020000
or pin=	11192190250000
or pin=	11201020240000
or pin=	10124190100000
or pin=	10124200070000
or pin=	10131040050000
or pin=	10131040150000
or pin=	10131050040000
or pin=	10131050200000
or pin=	10131090490000
or pin=	10131100010000
or pin=	10131150350000
or pin=	10131170030000
or pin=	10131170410000
or pin=	10131210020000
or pin=	10131210400000
or pin=	10132010090000
or pin=	10132100160000
or pin=	10132110300000
or pin=	10132130080000
or pin=	10132140030000
or pin=	10132140410000
or pin=	10132170030000
or pin=	10132170050000
or pin=	11194140350000
or pin=	11183290050000
or pin=	11191090090000
or pin=	11193000150000
or pin=	11193000390000
or pin=	11193020080000
or pin=	11193030150000
or pin=	11193080320000
or pin=	11193110030000
or pin=	11193120060000
or pin=	11193120280000
or pin=	11193120290000
or pin=	11193150160000
or pin=	11193170120000
or pin=	11193190050000
or pin=	10132180210000
or pin=	10134030290000
or pin=	10134100110000
or pin=	10242030080000
or pin=	10242070040000
or pin=	11191060330000
or pin=	11191120250000
or pin=	11191180270000
or pin=	11191190070000
or pin=	11191190200000
or pin=	11191200100000
or pin=	10244000090000
or pin=	10244010210000
or pin=	10244020170000
or pin=	10244050070000
or pin=	10244050080000
or pin=	10244080200000
or pin=	10244080420000
or pin=	10244100110000
or pin=	10244130310000
or pin=	10244130370000
or pin=	10244180340000
or pin=	10244180390000
or pin=	10244230100000
or pin=	10252050250000
or pin=	10252070340000
or pin=	10252070440000
or pin=	10252110080000
or pin=	10252110230000
or pin=	11301090350000
or pin=	11301090440000
or pin=	11301100100000
or pin=	11301100220000
or pin=	11301100560000
or pin=	11302010170000
or pin=	5333080160000
or pin=	5333210490000
or pin=	5334140050000
or pin=	5334180170000
or pin=	5334250310000
or pin=	5334300020000
or pin=	5343170080000
or pin=	5343240220000
or pin=	5344200200000
or pin=	5344230160000
or pin=	5344240250000
or pin=	5353110040000
or pin=	10133000190000
or pin=	10133010080000
or pin=	10133040090000
or pin=	10133050080000
or pin=	10133050310000
or pin=	10133080020000
or pin=	10133080050000
or pin=	10133130110000
or pin=	10133130140000
or pin=	10133130210000
or pin=	10133150270000
or pin=	10133160140000
or pin=	10133160150000
or pin=	10133210280000
or pin=	10134000030000
or pin=	10134000130000
or pin=	10134060050000
or pin=	10134070090000
or pin=	10134190250000
or pin=	10134190400000
or pin=	10134250360000
or pin=	10134260300000
or pin=	10134270800000
or pin=	10241010220000
or pin=	10241010250000
or pin=	10241010300000
or pin=	10241010320000
or pin=	10241020600000
or pin=	10241040100000
or pin=	10241050100000
or pin=	10241060320000
or pin=	10241060340000
or pin=	10241100360000
or pin=	10241110010000
or pin=	10241110020000
or pin=	10241120040000
or pin=	10241120110000
or pin=	10241120300000
or pin=	10241160650000
or pin=	10241161130000
or pin=	10241161220000
or pin=	10241161700000
or pin=	10241170090000
or pin=	10241170320000
or pin=	10241180120000
or pin=	10241180240000
or pin=	10241210320000
or pin=	10241210400000
or pin=	10241220220000
or pin=	10241220240000
or pin=	10241220380000
or pin=	10241230070000
or pin=	10241230540000
or pin=	10241230690000
or pin=	10243020130000
or pin=	10243020560000
or pin=	10243030080000
or pin=	10243030130000
or pin=	10243030300000
or pin=	10243030350000
or pin=	10243030390000
or pin=	10243040100000
or pin=	10243040370000
or pin=	10243040420000
or pin=	10243040470000
or pin=	10243040490000
or pin=	10243050390000
or pin=	10243060240000
or pin=	10243060250000
or pin=	10243080190000
or pin=	10243090050000
or pin=	10243090310000
or pin=	10243090320000
or pin=	10243090350000
or pin=	10243090430000
or pin=	10243090560000
or pin=	10243090590000
or pin=	10243090650000
or pin=	10243090660000
or pin=	10243110050000
or pin=	10243110170000
or pin=	10243110360000
or pin=	10243110380000
or pin=	10243110400000
or pin=	10243110470000
or pin=	10243120410000
or pin=	10243120450000
or pin=	10243120460000
or pin=	10243120480000
or pin=	10243130030000
or pin=	10243130380000
or pin=	10243130470000
or pin=	10243140190000
or pin=	10243140230000
or pin=	10243140240000
or pin=	10243140250000
or pin=	10243140430000
or pin=	10243150180000
or pin=	10243150570000
or pin=	10243160060000
or pin=	10243160390000
or pin=	10243170470000
or pin=	10243170550000
or pin=	10243170610000
or pin=	10243180090000
or pin=	10243180320000
or pin=	10111030020000
or pin=	10111030060000
or pin=	10111050050000
or pin=	10111050440000
or pin=	10112000060000
or pin=	10113120200000
or pin=	10242010030000
or pin=	10242010110000
or pin=	10242010140000
or pin=	10242010250000
or pin=	10242010290000
or pin=	10242020200000
or pin=	10242090040000
or pin=	10242100110000
or pin=	10242140110000
or pin=	10242140350000
or pin=	10242150120000
or pin=	10242170150000
or pin=	10242170220000
or pin=	10251060120000
or pin=	10251120020000
or pin=	10252201520000
or pin=	10252210030000
or pin=	11301130110000
or pin=	11301130240000
or pin=	11301140100000
or pin=	11301180140000
or pin=	11301200060000
or pin=	11302090080000
or pin=	4012000200000
or pin=	4014030200000
or pin=	4014040100000
or pin=	4014090390000
or pin=	4014100220000
or pin=	4014120330000
or pin=	4014120690000
or pin=	5063080430000
or pin=	4122010070000
or pin=	4122080480000
or pin=	4122110150000
or pin=	5063000110000
or pin=	5063010060000
or pin=	5063010070000
or pin=	5063040160000
or pin=	5064000180000
or pin=	5064000210000
or pin=	5064020190000
or pin=	5064060540000
or pin=	5064080020000
or pin=	5072070100000
or pin=	5072130460000
or pin=	5072170360000
or pin=	5083000030000
or pin=	5083130150000
or pin=	5083190040000
or pin=	5083190300000
or pin=	5083200140000
or pin=	5171030070000
or pin=	5171030080000
or pin=	5171030100000
or pin=	5172001020000
or pin=	5161050080000
or pin=	5171060390000
or pin=	5171070520000
or pin=	5171140030000
or pin=	5171140040000
or pin=	5171180520000
or pin=	5171180740000
or pin=	5173000230000
or pin=	5173000330000
or pin=	5173000490000
or pin=	5173010120000
or pin=	5173070440000
or pin=	5173090130000
or pin=	5173110130000
or pin=	5174000130000
or pin=	5174010010000
or pin=	5174010240000
or pin=	5174040130000
or pin=	5174040150000
or pin=	5174050130000
or pin=	5174050270000
or pin=	5174050320000
or pin=	5174120010000
or pin=	5174130040000
or pin=	5174130070000
or pin=	5174130490000
or pin=	5174150080000
or pin=	5174150100000
or pin=	5211030090000
or pin=	5211130050000
or pin=	5211190090000
or pin=	5211320020000
or pin=	5211320090000
or pin=	5213030020000
or pin=	5213060130000
or pin=	5213130060000
or pin=	5213180050000
or pin=	5214020210000
or pin=	5214140090000
or pin=	5214140360000
or pin=	5074010020000
or pin=	5074010190000
or pin=	5074050040000
or pin=	5074080060000
or pin=	5074110100000
or pin=	5171000180000
or pin=	5182000060000
or pin=	5182030010000
or pin=	5182050220000
or pin=	5182100040000
or pin=	5182110060000
or pin=	5182110070000
or pin=	5182150050000
or pin=	5182170110000
or pin=	5182210010000
or pin=	5182210110000
or pin=	5182240040000
or pin=	5182240190000
or pin=	5182250150000
or pin=	5182250200000
or pin=	5182260280000
or pin=	5074090220000
or pin=	5074130110000
or pin=	5074140010000
or pin=	5171080150000
or pin=	5063090050000
or pin=	5063090590000
or pin=	5063140270000
or pin=	5071020020000
or pin=	5071100080000
or pin=	5071110190000
or pin=	5071110330000
or pin=	5071110430000
or pin=	5071120130000
or pin=	5071120200000
or pin=	5072000120000
or pin=	5073070080000
or pin=	5201030080000
or pin=	5201090030000
or pin=	5201130050000
or pin=	5201210150000
or pin=	5201210160000
or pin=	5201210200000
or pin=	5202000370000
or pin=	5202010220000
or pin=	5202060210000
or pin=	5202100270000
or pin=	5202160130000
or pin=	5202200170000
or pin=	5202200310000
or pin=	5202200320000
or pin=	5202220080000
or pin=	5202250010000
or pin=	5201040270000
or pin=	5201190130000
or pin=	5203000370000
or pin=	5203000390000
or pin=	5181010150000
or pin=	5181010210000
or pin=	5181040340000
or pin=	5181040420000
or pin=	5181070300000
or pin=	5183080200000
or pin=	5184010020000
or pin=	5173120040000
or pin=	5174100100000
or pin=	5203180310000
or pin=	5204000610000
or pin=	5204070300000
or pin=	5213210370000
or pin=	5291000080000
or pin=	5291000380000
or pin=	5291010450000
or pin=	5291020190000
or pin=	5291020920000
or pin=	5292010160000
or pin=	5293000240000
or pin=	5202270070000
or pin=	5202280110000
or pin=	5204010020000
or pin=	5204050280000
or pin=	5204100070000
or pin=	5213000500000
or pin=	5213110150000
or pin=	5213110210000
or pin=	5213120190000
or pin=	5213120300000
or pin=	5213220240000
or pin=	5213220310000
or pin=	5281010030000
or pin=	5282070240000
or pin=	5282080260000
or pin=	5283070470000
or pin=	5283080230000
or pin=	5283100030000
or pin=	5283100120000
or pin=	5283100150000
or pin=	5283120070000
or pin=	5283120150000
or pin=	5283140100000
or pin=	5283140160000
or pin=	5283150020000
or pin=	5283160200000
or pin=	5283160240000
or pin=	5284150310000
or pin=	5284170020000
or pin=	5284170050000
or pin=	5284190050000
or pin=	5284230120000
or pin=	5284240150000
or pin=	5284250360000
or pin=	5294010280000
or pin=	5294010390000
or pin=	5294140020000
or pin=	5331000260000
or pin=	5331000580000
or pin=	5331010060000
or pin=	5331020390000
or pin=	5331030510000
or pin=	5331040050000
or pin=	5331050190000
or pin=	5331070110000
or pin=	5331070120000
or pin=	5331070130000
or pin=	5332040140000
or pin=	5332110150000
or pin=	5332180290000
or pin=	5334020710000
or pin=	5334030260000
or pin=	5334030370000
or pin=	5334040060000
or pin=	5341000130000
or pin=	5341000180000
or pin=	5341040030000
or pin=	5341080200000
or pin=	5341120100000
or pin=	5341150220000
or pin=	5341170180000
or pin=	5341210530000
or pin=	5341210750000
or pin=	5343000140000
or pin=	5343010250000
or pin=	5343050160000
or pin=	5343070240000
or pin=	5343070260000
or pin=	5343080250000
or pin=	5353020150000
or pin=	5353020200000
or pin=	5342020190000
or pin=	5342050150000
or pin=	5342070070000
or pin=	5342110100000
or pin=	5342130120000
or pin=	5342160030000
or pin=	5342170030000
or pin=	5342190100000
or pin=	5342210210000
or pin=	5342230020000
or pin=	5344010050000
or pin=	5351030190000
or pin=	5351070550000
or pin=	5351080140000
or pin=	5351140130000
or pin=	5351150320000
or pin=	5351150880000
or pin=	5351180140000
or pin=	5351190140000
or pin=	5351190150000
or pin=	5304060860000
or pin=	5304070310000
or pin=	5312290490000
or pin=	5314080760000
or pin=	5314200220000
or pin=	5314230120000
or pin=	5321140320000
or pin=	5323030100000
or pin=	5271100050000
or pin=	5271120030000
or pin=	5271130360000
or pin=	5273000470000
or pin=	5273000480000
or pin=	5282030040000
or pin=	5284000070000
or pin=	5284000170000
or pin=	5284010180000
or pin=	5284010210000
or pin=	5284010300000
or pin=	5284010350000
or pin=	5193050180000
or pin=	5193100190000
or pin=	5193100210000
or pin=	5193100290000
or pin=	5193100730000
or pin=	5193110110000
or pin=	5193130140000
or pin=	5193150160000
or pin=	5193150290000
or pin=	5193150310000
or pin=	5193180010000
or pin=	5193230120000
or pin=	5193250370000
or pin=	5194130070000
or pin=	5194130130000
or pin=	5194130150000
or pin=	5301020120000
or pin=	5303010250000
or pin=	5303020030000
or pin=	5304000110000
or pin=	5304000330000
or pin=	5311110460000
or pin=	5312070210000
or pin=	5312150200000
or pin=	5313050010000
or pin=	5313070330000
or pin=	5313120430000
or pin=	5313140190000
or pin=	5313230400000
or pin=	5314040400000
or pin=	5314060190000
or pin=	5314210210000
or pin=	5203130200000
or pin=	5203150040000
or pin=	5293150260000
or pin=	5293160480000
or pin=	5293160590000
or pin=	5294100250000
or pin=	5294150110000
or pin=	5294200020000
or pin=	5294200060000
or pin=	5294260060000
or pin=	5302010180000
or pin=	5302010470000
or pin=	5302010500000
or pin=	5302010520000
or pin=	5302020470000
or pin=	5273090180000
or pin=	5273180040000
or pin=	5274001050000
or pin=	5274001060000
or pin=	5274001240000
or pin=	5274080030000
or pin=	5274090130000
or pin=	5274120050000
or pin=	5274160170000
or pin=	5321040370000
or pin=	5321100060000
or pin=	5321100090000
or pin=	5321150070000
or pin=	5322000700000
or pin=	5322001120000
or pin=	5322001800000
or pin=	5322010310000
or pin=	5322030390000
or pin=	5323060690000
or pin=	5323100010000
or pin=	5323100190000
or pin=	5323100200000
or pin=	5323110420000
or pin=	5323120060000
or pin=	5323120100000
or pin=	5324060130000
or pin=	5324070030000
or pin=	5324080120000
or pin=	5331110470000
or pin=	5333000140000
or pin=	5333220010000
or pin=	5333220200000
or pin=	5334000090000
or pin=	5334000120000
or pin=	5062010610000
or pin=	5062011050000
or pin=	5062011080000
or pin=	5081000080000
or pin=	5083140170000
or pin=	5261010070000
or pin=	5062010420000
or pin=	5083140290000
or pin=	5161060340000
or pin=	5172010030000
or pin=	5214120090000
or pin=	5274040010000
or pin=	5274040020000
or pin=	5274040030000
or pin=	5274040100000) bs=1.
*select if bs=0.


Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute tnb=(town*1000) + nghcde.

if (tnb= 17011 ) n=3.89.
if (tnb= 17013 ) n=5.49.
if (tnb= 17020 ) n=7.74.
if (tnb= 17041 ) n=4.78.
if (tnb= 17042 ) n=9.11.
if (tnb= 17043 ) n=10.50.
if (tnb= 17050 ) n=15.67.
if (tnb= 17060 ) n=3.13.
if (tnb= 17071 ) n=4.95.
if (tnb= 17080 ) n=5.08.
if (tnb= 17090 ) n=5.76.
if (tnb= 17110 ) n=3.25.
if (tnb= 17112 ) n=3.69.
if (tnb= 17120 ) n=5.82.
if (tnb= 17130 ) n=2.79. 
if (tnb= 17140 ) n=2.72. 
if (tnb= 17200 ) n=5.23.
if (tnb= 17210 ) n=3.77.
if (tnb= 17220 ) n=3.13.
if (tnb= 23010 ) n=11.14.
if (tnb= 23011 ) n=6.71.
if (tnb= 23021 ) n=12.38.
if (tnb= 23022 ) n=12.21.
if (tnb= 23041 ) n=8.75.
if (tnb= 23042 ) n=8.21.
if (tnb= 23043 ) n=12.00.
if (tnb= 23044 ) n=11.04.
if (tnb= 23045 ) n=7.08.
if (tnb= 23050 ) n=13.08.
if (tnb= 23080 ) n=19.25.
if (tnb= 23091 ) n=12.19.
if (tnb= 23092 ) n=8.52.
if (tnb= 23093 ) n=6.60.
if (tnb= 23094 ) n=8.70.
if (tnb= 23100 ) n=4.17.
if (tnb= 23110 ) n=16.00.
if (tnb= 23120 ) n=3.74.
if (tnb= 23132 ) n=10.36.
if (tnb= 23141 ) n=11.25.
if (tnb= 23150 ) n=4.84.
if (tnb= 23170 ) n=14.60.
if (tnb= 23171 ) n=36.01.


*COMPUTE FX = cumfile13141516.
*COMPUTE SRFX = sqrt(fx).
*RECODE FX (SYSMIS=0).


*select if puremarket=1.

compute sb17011=0.
compute sb17013=0.
compute sb17020=0.
compute sb17041=0.
compute sb17042=0.
compute sb17043=0.
compute sb17050=0.
compute sb17060=0.
compute sb17071=0.
compute sb17080=0.
compute sb17090=0.
compute sb17110=0.
compute sb17112=0.
compute sb17120=0.
compute sb17130=0.
compute sb17140=0. 
compute sb17200=0.
compute sb17210=0.
compute sb17220=0.
compute sb23010=0.
compute sb23011=0.
compute sb23021=0.
compute sb23022=0.
compute sb23041=0.
compute sb23042=0.
compute sb23043=0.
compute sb23044=0.
compute sb23045=0.
compute sb23050=0.
compute sb23080=0.
compute sb23091=0.
compute sb23092=0.
compute sb23093=0.
compute sb23094=0.
compute sb23100=0.
compute sb23110=0.
compute sb23120=0.
compute sb23132=0.
compute sb23141=0.
compute sb23150=0.
compute sb23170=0.
compute sb23171=0.

compute n17011=0.
compute n17013=0.
compute n17020=0.
compute n17041=0.
compute n17042=0.
compute n17043=0.
compute n17050=0.
compute n17060=0.
compute n17071=0.
compute n17080=0.
compute n17090=0.
compute n17110=0.
compute n17112=0.
compute n17120=0.
compute n17130=0.
compute n17140=0. 
compute n17200=0.
compute n17210=0.
compute n17220=0.
compute n23010=0.
compute n23011=0.
compute n23021=0.
compute n23022=0.
compute n23041=0.
compute n23042=0.
compute n23043=0.
compute n23044=0.
compute n23045=0.
compute n23050=0.
compute n23080=0.
compute n23091=0.
compute n23092=0.
compute n23093=0.
compute n23094=0.
compute n23100=0.
compute n23110=0.
compute n23120=0.
compute n23132=0.
compute n23141=0.
compute n23150=0.
compute n23170=0.
compute n23171=0.


if (tnb= 17011 ) n17011=1.
if (tnb= 17013 ) n17013=1.
if (tnb= 17020 ) n17020=1.
if (tnb= 17041 ) n17041=1.
if (tnb= 17042 ) n17042=1.
if (tnb= 17043 ) n17043=1.
if (tnb= 17050 ) n17050=1.
if (tnb= 17060 ) n17060=1.
if (tnb= 17071 ) n17071=1.
if (tnb= 17080 ) n17080=1.
if (tnb= 17090 ) n17090=1.
if (tnb= 17110 ) n17110=1.
if (tnb= 17112 ) n17112=1.
if (tnb= 17120 ) n17120=1.
if (tnb= 17130 ) n17130=1.
if (tnb= 17140 ) n17140=1.
if (tnb= 17200 ) n17200=1.
if (tnb= 17210 ) n17210=1.
if (tnb= 17220)  n17220=1.
if (tnb= 23010 ) n23010=1.
if (tnb= 23011 ) n23011=1.
if (tnb= 23021 ) n23021=1.
if (tnb= 23022 ) n23022=1.
if (tnb= 23041 ) n23041=1.
if (tnb= 23042 ) n23042=1.
if (tnb= 23043 ) n23043=1.
if (tnb= 23044 ) n23044=1.
if (tnb= 23045 ) n23045=1.
if (tnb= 23050 ) n23050=1.
if (tnb= 23080 ) n23080=1.
if (tnb= 23091 ) n23091=1.
if (tnb= 23092 ) n23092=1.
if (tnb= 23093 ) n23093=1.
if (tnb= 23094 ) n23094=1.
if (tnb= 23100 ) n23100=1.
if (tnb= 23110 ) n23110=1.
if (tnb= 23120 ) n23120=1.
if (tnb= 23132 ) n23132=1.
if (tnb= 23141 ) n23141=1.
if (tnb= 23150 ) n23150=1.
if (tnb= 23170 ) n23170=1.
if (tnb= 23171 ) n23171=1.

if (tnb=17011) sb17011=sqrt(bsf).
if (tnb=17013) sb17013=sqrt(bsf).
if (tnb=17020) sb17020=sqrt(bsf).
if (tnb=17041) sb17041=sqrt(bsf).
if (tnb=17042) sb17042=sqrt(bsf).
if (tnb=17043) sb17043=sqrt(bsf).
if (tnb=17050) sb17050=sqrt(bsf).
if (tnb=17060) sb17060=sqrt(bsf).
if (tnb=17071) sb17071=sqrt(bsf).
if (tnb=17080) sb17080=sqrt(bsf).
if (tnb=17090) sb17090=sqrt(bsf).
if (tnb=17110) sb17110=sqrt(bsf).
if (tnb=17112) sb17112=sqrt(bsf).
if (tnb=17120) sb17120=sqrt(bsf).
if (tnb=17130) sb17130=sqrt(bsf).
if (tnb=17140) sb17140=sqrt(bsf).
if (tnb=17200) sb17200=sqrt(bsf).
if (tnb=17210) sb17210=sqrt(bsf).
if (tnb=17220) sb17220=sqrt(bsf).
if (tnb=23010) sb23010=sqrt(bsf).
if (tnb=23011) sb23011=sqrt(bsf).
if (tnb=23021) sb23021=sqrt(bsf).
if (tnb=23022) sb23022=sqrt(bsf).
if (tnb=23041) sb23041=sqrt(bsf).
if (tnb=23042) sb23042=sqrt(bsf).
if (tnb=23043) sb23043=sqrt(bsf).
if (tnb=23044) sb23044=sqrt(bsf).
if (tnb=23045) sb23045=sqrt(bsf).
if (tnb=23050) sb23050=sqrt(bsf).
if (tnb=23080) sb23080=sqrt(bsf).
if (tnb=23091) sb23091=sqrt(bsf).
if (tnb=23092) sb23092=sqrt(bsf).
if (tnb=23093) sb23093=sqrt(bsf).
if (tnb=23094) sb23094=sqrt(bsf).
if (tnb=23100) sb23100=sqrt(bsf).
if (tnb=23110) sb23110=sqrt(bsf).
if (tnb=23120) sb23120=sqrt(bsf).
if (tnb=23132) sb23132=sqrt(bsf).
if (tnb=23141) sb23141=sqrt(bsf).
if (tnb=23150) sb23150=sqrt(bsf).
if (tnb=23170) sb23170=sqrt(bsf).
if (tnb=23171) sb23171=sqrt(bsf).


compute evn=0.
compute newt=0.
if town=17 evn=1.
if town=23 newt=1.

Compute lowzoneevn=0.
if town=17 and  (nghcde=11 or nghcde=13  or nghcde=20  or nghcde=41 or nghcde=42
or nghcde=43 or nghcde=50 or nghcde=71 or nghcde=80 or nghcde=90 
or nghcde=120 or nghcde=200)  lowzoneevn=1.                         	

Compute midzoneevn=0.
if town=17 and (nghcde=110 or nghcde=112 or nghcde=140 or nghcde=210)  midzoneevn=1. 

Compute highzoneevn=0.
if town=17 and (nghcde=60 or nghcde=130 or nghcde=220)  highzoneevn=1.


*Compute srfxlowblockevn=0.
*if lowzoneevn=1 srfxlowblockevn=srfx*lowzoneevn.

*Compute srfxmidblockevn=0.
*if midzoneevn=1 srfxmidblockevn=srfx*midzoneevn.

*Compute srfxhighblockevn=0.
*if highzoneevn=1 srfxhighblockevn=srfx*highzoneevn.


Compute midzonenewt=0.
if town=23 and (nghcde=11 or nghcde=100 or nghcde=120)  midzonenewt=1. 

Compute lowzonenewt=0.
if (town=23 and  midzonenewt=0)  lowzonenewt=1.                         	


*Compute srfxlowblocknewt=0.
*if lowzonenewt=1 srfxlowblocknewt=srfx*lowzonenewt.

*Compute srfxmidblocknewt=0.
*if midzonenewt=1 srfxmidblocknewt=srfx*midzonenewt.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.

if (lsf  > 6000)     lsf = 6000 + (lsf - 6000)/3.

if class=95 and tnb=17011  	lsf=3980.
if class=95 and tnb=17013  	lsf=2814.
if class=95 and tnb=17060  	lsf=2315.
if class=95 and tnb=17071  	lsf=926.
if class=95 and tnb=17080  	lsf=1783.
if class=95 and tnb=17110  	lsf=2789.
if class=95 and tnb=17112 	lsf=2789.
if class=95 and tnb=17120  	lsf=2916.
if class=95 and tnb=17130  	lsf=3006.
if class=95 and tnb=17140  	lsf=2564.
if class=10 and tnb=17140   lsf=2964.
if class=95 and tnb=17220  lsf=2362.
if class=95 and tnb=23093  	lsf=2621.
if class=95 and tnb=23094 	lsf=1313.
if class=95 and tnb=23100  	lsf=2462.
if class=95 and tnb=23120  	lsf=1855.
if class=95 and tnb=23150  	lsf=2160.

compute nsrevnl=0.
compute nsrnewtl=0.
if evn=1 nsrevnl=n*evn*sqrt(lsf).
if newt=1 nsrnewtl=n*newt*sqrt(lsf).
compute srbsf=sqrt(bsf).
compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*sqrt(bsf).
compute masbsf=mason*sqrt(bsf).
Compute stubsf=stucco*sqrt(bsf).
Compute frmsbsf=framas*sqrt(bsf).
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute srage=sqrt(age).
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

if firepl >= 2 firepl=2.
compute nfirepl=n*firepl.


Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute nsitebsf=nsiteben*bsf.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.

select if town=17.

compute mv = -48792.70752	
+ 6296.475999*nfirepl
- 4673.944037*nsrage
+ 4241.783751*nbathsum
+ 181.888849*nbsf1095
+ 14267.4754*sb23093
+ 14095.10526*sb23094
+ 231.96319*nbsf1112
+ 4905.254458*sb23050
+ 6209.776589*sb23010
- 6055.32001*garnogar
- 3025.0777*garage1
+ 201.1177691*nbsf34
+ 359.7127165*nsrnewtl
+ 1094.661849*nsrevnl
+ 426.3209022*nbsf234
+ 2.644692706*nsitebsf
+ 13402.11959*sb23141
+ 12572.83759*sb23092
+ 12283.45463*sb23022
+ 2.028148622*nprembsf
+ 12600.64316*sb23041
+ 12457.50766*sb23091
+ 11705.12268*sb23150
- 9382.398475*sb17050
+ 472.5319255*nbsf778
+ 11105.22896*sb23044
+ 11564.15615*sb23110
+ 12195.15927*sb23045
+ 3565.294581*sb17060
+ 9766.373862*sb23021
+ 4981.779289*sb17110
+ 45969.95475*biggar
+ 9063.638741*sb23132
+ 10819.6847*sb23120
- 1234.538208*nbaspart
+ 9740.157003*sb23011
+ 8336.070125*sb23170
+ 7924.948313*sb23043
+ 10988.35606*sb23042
+ 9952.784468*sb23100
+ 7660.425368*sb17041
+ 5797.51966*sb17013
+ 5261.946672*sb17120
+ 6492.823447*sb17071
+ 5884.309032*sb17080
+ 6253.043058*sb17200
+ 4299.17858*sb17090
+ 6284.025744*sb17011
+ 6868.834767*sb17130
+ 5099.578832*sb17112
+ 6121.349926*sb17210
+ 5322.713802*sb17220
- 1655.231998*nnobase
+ 680.4971303*nbsf89
+ 588.7720303*nbsf56
+ 2681.208943*nbasfull
+ 0.060711817*nbsfair
+ 5467.01426*garage2.


 

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv17.sav'/keep town pin mv. 

