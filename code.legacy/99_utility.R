U_msg <- function(...) {
	cat(...,sep="");flush.console();
}
U_hr <- function(n=80,ch="-",newline=TRUE) {
	U_msg(paste(rep(ch,n),collapse=""),ifelse(newline,"\n",""));
}
U_loop <- function(iter,loop,header="",...) {
	min2pretty <- function(n) {
		paste(formatC(floor(n),width=3),":",formatC(round(60*(n-floor(n))),flag="0",width=2),sep="");
	}
	objname <- ".lt";
	
	# Start of loop
	if (iter==loop[1]) {
		assign(objname,Sys.time(),envir=.GlobalEnv);
		U_msg(header,"Loop init: ",iter,"/",loop[length(loop)]);
    } else {
		if (!exists(objname,envir=.GlobalEnv)) {	# Error control
			assign(objname,Sys.time(),envir=.GlobalEnv);
		}
		timer <- get(objname,envir=.GlobalEnv);
		elapsed <- as.numeric(difftime(Sys.time(),timer,units="mins"));
		
    	completed <- match(iter,loop);
    	remaining <- length(loop)-completed;
    	status <- (completed)/length(loop);
		remaintime <- elapsed*remaining/completed;
		
    	last <- loop[length(loop)];
    	iter <- formatC(iter,width=nchar(last));
    	U_msg(paste(rep("\b",140),collapse=""),header,"Loop: ",iter,"/",last," (",round(100*status),"%)",...,
    		" (elapsed = ",min2pretty(elapsed)," M:S,",
			" EST TOTAL = ",min2pretty(elapsed+remaintime),
    		" (remain = ",min2pretty(remaintime),"))");
    }
    invisible(NULL);
}

U_find <- function(str,direc="code.r/",...) {
	# Get all R files
	files <- dir(direc);
	files_ext <- toupper(sapply(strsplit(files,"\\."),function(vec) vec[length(vec)]));
	files <- subset(files,files_ext=="R");

	# Read them in and check
	found.fun <- found.use <- list();
	for (iter in seq_along(files)) {
		fn <- paste(direc,files[iter],sep="");
		code <- scan(fn,what=character(0),sep="\n",blank.lines.skip=FALSE,quiet=TRUE);

		# Locate any instances
		IX <- grep(str,code,...);
		if (length(IX)>0) {
			# Check for function definition
			ix <- intersect(IX,grep("function\\(",code));
			if (length(ix)>0) {
				string <- code[ix];
				string <- gsub("\t","   ",string);
				found.fun[[length(found.fun)+1]] <- data.frame(row=ix,str=string,stringsAsFactors=FALSE);
				names(found.fun)[length(found.fun)] <- files[iter];
			}
			# Check for general usage
			ix <- setdiff(IX,ix);
			if (length(ix)>0) {
				string <- code[ix];
				string <- gsub("\t","   ",string);
				found.use[[length(found.use)+1]] <- data.frame(row=ix,str=string,stringsAsFactors=FALSE);
				names(found.use)[length(found.use)] <- files[iter];
			}
		}
	}

	print("Found.use");
	print(found.use);
	U_hr();U_hr();U_hr();
	print("Found.fun");
	print(found.fun);
}