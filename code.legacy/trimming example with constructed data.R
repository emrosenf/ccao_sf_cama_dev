# Example of trimming using constructed data
# Robert Ross, raross1217@gmail.com, 2018/11/27

# 3 parts to this example. First, we define 'clean' data and show that the constructued parameters 
# can be recovered using OLS
# The, we dirty the data and see what impact it has on our OLS estimates
# Finally, we see if we can trim out the dirty observations and recover our original parameters

# Setup

rm(list=ls());flush.console(); theme_set(theme_minimal());

libs <- c("ggplot2", "scales")

# Refresh ----
refresh <- function(recode=TRUE){
  # Refresh packages
  check.packages <- function(libs){
    new.libs <- libs[!(libs %in% installed.packages()[, "Package"])]
    if (length(new.libs)) 
      install.packages(new.libs, dependencies = TRUE)
    sapply(libs, require, character.only = TRUE)
  }
  for (iter in 1:length(libs))
    check.packages(libs[iter]);
  rm(iter)
}
refresh()

# Empty data frame
set.seed(7677)
example_data <- data.frame(
  fake_pin = numeric(100)
  , price = numeric(100)
  , bedrooms = numeric(100))
example_data$fake_pin <- seq.int(nrow(example_data))
# Define  underlying structure of relationship
# price = B0 + B1*building_sqr_ft + error
# We set B0=100,000 and B1=$50/sqr ft
example_data$building_sqr_ft <- rnorm(100,mean = 2500, sd = 10)
example_data$constant <- 100000
example_data$error <- rnorm(100, mean = 0, sd = 1000)
example_data$price <- example_data$constant + 50*example_data$building_sqr_ft + example_data$error

# visualize
hist(example_data$price)
hist(example_data$building_sqr_ft)

# Recover structural parameters with regression
f1 <- price ~ building_sqr_ft
model_1 <- lm(f1, data=example_data)
summary(model_1) # Great - we get a coefficient estimate of ~53, very close to what we know to be the actual parameter

example_data$fitted_value <- predict(model_1)

# In this simple construction, the fitted values are just the points along the slope of the fitted line
plot_1 <- ggplot(example_data, aes(x=example_data$building_sqr_ft, y=example_data$price))+
  geom_point()+
  geom_smooth(method='lm')+
  scale_y_continuous(label=comma)+
  labs( title = "Fitted values and actual data"
        , subtitle = "Using constructed data"
        , x = "$ per square foot"
        , y = "Sale price")

# How about overall performance, COD and PRD?
# First calculate ratios
example_data$ratio <- NA
example_data$ratio <-example_data$fitted_value/example_data$price

# Use your ratios to get performance stats
cod_benchmark <- sum(abs(example_data$ratio-median(example_data$ratio))) / (nrow(example_data)*median(example_data$ratio))
prd_benchmark <- mean(example_data$ratio)/weighted.mean(example_data$ratio, example_data$price)
cod_benchmark # Absurdly good. 
prd_benchmark # Basically perfect.
# We wish, right?

# Now, let's dirty up the data. Suppose some properties' sale prices are way above what you'd expect given
# the size of the house. Maybe because famous moves were filed at these houses? 
example_data$movie_bonus <- 0
example_data$movie_home <- 0

example_data$movie_bonus <- rnorm(100, mean=5000, sd = 100)
example_data$movie_home <- factor(as.numeric(example_data$fake_pin <=5))
example_data$price <- ifelse(example_data$movie_home==1,
    example_data$price + example_data$movie_bonus,
    example_data$constant + 50*example_data$building_sqr_ft + example_data$error)

# Estimate again
model_2 <- lm(f1, data=example_data)
summary(model_2)
example_data$fitted_value <- predict(model_2)
# Bad - your estimate of B1 is ~20% off the mark
# Visualize your outliers - the red line is the parameter estimates we want
plot_2 <- ggplot(example_data, 
  aes(x=example_data$building_sqr_ft
    , y=example_data$price
    , group = example_data$movie_home
    , colour =  example_data$movie_home
    , shape = example_data$movie_home))+
  geom_point()+
  geom_smooth(method='lm')+
  scale_shape_discrete(name  ="Data",
                       breaks=c(0,1),
                       labels=c("Good data", "Constructed Outliers"))+
  scale_color_discrete(name  ="Data",
                       breaks=c(0,1),
                       labels=c("Good data", "Constructed Outliers"))+  
  scale_y_continuous(label=comma)+
  labs( title = "Fitted values and actual data"
        , subtitle = "Using constructed data with 5% outliers"
        , x = "$ per square foot"
        , y = "Sale price")

# How do your CODs look?
example_data$ratio <- NA
example_data$ratio <-example_data$fitted_value/example_data$price

# Use your ratios to get performance stats
cod_dirtydata <- sum(abs(example_data$ratio-median(example_data$ratio))) / (nrow(example_data)*median(example_data$ratio))
prd_dirtydata <- mean(example_data$ratio)/weighted.mean(example_data$ratio, example_data$price)
(cod_benchmark - cod_dirtydata)/cod_benchmark 
(prd_benchmark - prd_dirtydata)/prd_benchmark
# So, while the cods are still really good, they have changed dramatically from their benchmark values.

# Can we trim these outliers out of our sample and get better results
# Want to normalize our ratios so that the trim is more even on each side of the dist.
mean <- mean(subset(example_data)$ratio)
std <- sd(subset(example_data)$ratio)
example_data$Nratio <- NA
example_data$Nratio <-(example_data$ratio-mean)/std
rm(mean, std)
# Calculate the IQR
hist(example_data$Nratio )
p25 <- quantile(example_data$Nratio, c(.25), names = FALSE)
p75 <- quantile(example_data$Nratio, c(.75), names = FALSE)
IQR <- abs(p75-p25)

# And trim based on IQR
example_data$modeling_filter <- factor(ifelse(
  example_data$Nratio<=p25-.75*IQR | example_data$Nratio>=p75+.75*IQR
  ,0,1
))
nrow(subset(example_data, modeling_filter==1))

plot_3 <- ggplot(example_data, 
                 aes(x=example_data$building_sqr_ft
                     , y=example_data$price
                     , group = example_data$modeling_filter
                     , colour =  example_data$modeling_filter
                     , shape = example_data$modeling_filter))+
  geom_point()+
  scale_shape_discrete(name  ="Data",
                       breaks=c(0,1),
                       labels=c("Filter-ID'd Outliers","Good data"))+
  scale_color_discrete(name  ="Data",
                       breaks=c(0,1),
                       labels=c("Filter-ID'd Outliers", "Good data"))+  
  scale_y_continuous(label=comma)+
  labs( title = "Fitted values and actual data"
        , subtitle = "Using constructed data with 5% outliers"
        , x = "$ per square foot"
        , y = "Sale price")

# How'd we do in terms of identifying the movie homes?
nrow(subset(example_data, modeling_filter==0 & movie_home==1)) # True positives
nrow(subset(example_data, modeling_filter==0 & movie_home==0)) # False positives  

# Now, we want to run our model on the TRIMMED data
model_3 <- lm(f1, data=subset(example_data, modeling_filter==1))
summary(model_3)
# Better, much closter to the truth

#Predict fitted values for ALL the sales
example_data$fitted_value <- predict(model_3, newdata = example_data)

# How do your CODs look?
example_data$ratio <- NA
example_data$ratio <-example_data$fitted_value/example_data$price

# Use your ratios to get performance stats
cod_trimmeddata <- sum(abs(example_data$ratio-median(example_data$ratio))) / (nrow(example_data)*median(example_data$ratio))
prd_trimmeddata <- mean(example_data$ratio)/weighted.mean(example_data$ratio, example_data$price)
cod_benchmark
cod_dirtydata
cod_trimmeddata
(cod_dirtydata - cod_trimmeddata)/cod_benchmark 
(prd_dirtydata - prd_trimmeddata)/prd_benchmark
(cod_benchmark - cod_trimmeddata)/cod_benchmark 
(prd_benchmark - prd_trimmeddata)/prd_benchmark

# Table results
example_summary <- rbind(
  data.frame(data = "Good data, not trimmed"
      , cod = cod_benchmark
      , prd = prd_benchmark
      , model_fit=summary(model_1)$adj.r.squared
      , sq_ft_dollars=summary(model_1)$coefficients[2,1]
      , false_positives = 0
      , false_negatives = 0
      , true_positives = 0
      , true_negatives = 100)
  , data.frame(data = "Dirty data, not trimmed"
      , cod = cod_dirtydata
      , prd = prd_dirtydata
      , model_fit=summary(model_2)$adj.r.squared
      , sq_ft_dollars=summary(model_2)$coefficients[2,1]
      , false_positives = 0
      , false_negatives = 5
      , true_positives = 0
      , true_negatives = 0)
  , data.frame(data = "Dirty data, trimmed"
      , cod = cod_trimmeddata
      , prd = prd_trimmeddata
      , model_fit=summary(model_3)$adj.r.squared
      , sq_ft_dollars=summary(model_3)$coefficients[2,1]
      , false_positives = nrow(subset(example_data, modeling_filter==0 & movie_home==0))
      , false_negatives = nrow(subset(example_data, modeling_filter==1 & movie_home==1))
      , true_positives = nrow(subset(example_data, modeling_filter==0 & movie_home==1))
      , true_negatives = nrow(subset(example_data, modeling_filter==1 & movie_home==0))
  )
)
example_summary
plot_1
plot_2
plot_3

