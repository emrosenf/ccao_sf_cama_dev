# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 

# Top comments ----

# This is the control file for the Cook County Assessor's valuations scripts.

# This script estimates market values many properties in Cook County.
# This script requires 32 GB RAM to run

# Version 0.2
# Written 12/26/2018
# Updated 1/31/2019
# Incorporated the changes in the other files

rm(list=ls())

# Controls ----
# Who is running this code?
user <- Sys.info()[['user']]
# Options 
options(scipen=999)
options(java.parameters = "-Xmx10000m")
# What packages will you need?
libs <- c("DBI", "odbc", "sqldf", "rgdal", "sf", "stringr", "xlsx", "maptools","sp","foreign","fields", "mgcv", "car","RPostgreSQL"
          , "rowr", "plyr","dplyr", "DBI", "RODBC", "RStata", "lattice", "RColorBrewer", 
          "ggplot2", "scales", "zoo", "leaflet", "htmlwidgets", "htmltools", "sjPlot", "gdata"
          ,"gbm", "progress", "knitr", "kableExtra","Hmisc", "quantreg","randomForest","geosphere", "ape", "SpatialNP", "spdep", "fts"
          , "nnet", "systemfit", "foreign", "Rcpp", "gstat", "gridExtra", "data.table", "ggrepel", "XLConnect", "MASS")

# Where are your files stored?
wd <- paste0("/Users/",user,"/development/gitlab/ccao_sf_cama_dev")
setwd(wd)
common.drive <- wd
dirs <- list(code=paste0(wd,"/code.r/")
             , data=paste0(common.drive,"/data/")
             , results=paste0(common.drive, "/results/")
             , documentation=paste0(common.drive, "/documentation/")
             , results_sandbox=paste0(common.drive, "/results/sandbox/")
             , final_values=paste0(common.drive, "/results/final_values/")
             , results_desk_review=paste0(common.drive, "/output_data/desktop_review/")
             , results_modeling=paste0(common.drive, "/results/modeling/")
             , results_ad_hoc=paste0(common.drive, "/results/ad_hoc/")
             , spatial_data=paste0(common.drive,"/data/spatial/")
             , parcels=paste0(common.drive,"/data/spatial/", "2018_parcels.shp")
             , raw_parcels=paste0(common.drive,"/data/spatial/", "TY2018_DefaultParcels.shp")
             , raw_tracts=paste0(common.drive,"/data/spatial/", "tl_2018_17_tract.shp")
             , bad_data=paste0(common.drive,"/data/bad_data/")
             , raw_data=paste0(common.drive,"/data/raw_data/")
             , utility=paste0(wd, "/code.r/utils/"))
             
rm(common.drive, wd)

# What townships are we pulling data for?
modeling_townships <- "10, 18, 29, 35"
# What township are we assessming?
target_township <- 10
# NORTH TRI = 17, 23, 10, 16, 18, 20, 22, , 24, 25, 26, 29, 30, 26 
# Rogers Park = 75
# What is the lower limit on sale dates?
min_sale_date <- "2013" 
# What is the date at which we want to estimate values?
estimation_date <-as.Date("01/01/2019", "%m/%d/%Y")
tax_year <- lubridate::year(estimation_date)
# For COD and PRD, how many bootstrapp iterations do you want (fewer yields less precise estiamtes, but takes less time)?
bootstrapp_iters <- 100
# Which post-modeling adjustments do you want to make?
adjust_regressivity <- "No" # because it doesn't work
adjust_med_ratios <- "Yes"
limit_excessive_ratios <- "Yes"
# How many rounds of trimming?
filter_iters <- 3
# When using random forest, gbm, how many trees?
ntree <- 10^3
# Check holdout sample? (y for "yes", n for "no")
holdout_switch <- "n"
# After first modeling iteration, how many observations do you want to sen to desktop review?
review_max <- 5*10^2
# Seed for random numbers
set.seed(7677)
# What method to use to value land?
m = c("l_8020")
land_val_method <- m[1]
rm(m)
# activare Don's neighborhood experiment? ('on' for "yes", 'off' for "no")
nbhd_experiment <- 'off'
# Template for integrity checks
integrity_checks <- data.frame(check="", outcome="")
# Load utilities
source(paste0(dirs$utility, "check.packages.R"))
# Refresh packages
check.packages(libs)
# Open ODBC connection (you stil have to manually connect)
pw <-"generalUser"
#CCAODATA <- dbConnect(odbc(), Driver = "SQL Server", Server = "10.128.57.154", 
#     Database = "CCAODATA", UID = "generalUser", PWD = "generalUser", 
#     Port = 1433)
# define scope filepath
if (nbhd_experiment == 'on'){
  destfile <- paste0(dirs$final_values, "NBHD_experiment_scope_",gsub(" ", "", gsub(",","_",target_township)), "_", tax_year, ".Rda")
}else{
  destfile <- paste0(dirs$final_values, "scope_",gsub(" ", "", gsub(",","_",target_township)), "_", tax_year, ".Rda")
}
# Every batch of townships goes through a number of steps in the modeling process
# The results of each step are saved in a global file called a 'scope'
# 
# if (file.exists(destfile)) {
#   print("You are about to overwrite an existing scope file.")
#   choices <- c("Yes, I'm sure!", "No, wait, better not.")
#   choice <- select.list(choices,title="Are you sure?",preselect=choices,multiple=FALSE)
#   if(choice=="Yes, I'm sure!"){
#     print(paste0("Over-writing ", destfile))
#     file_info <- update_file_info('overwriting scope')
#     save(dirs, integrity_checks, file_info, file=destfile)
#     }else{
#     print("Program halted")
#   }
# }else{
#   file_info <- update_file_info('scope created')
#   save(dirs, integrity_checks, file_info, file=destfile)
#   print("Scope created")
# }

### Property valuation ----

# Create XY data
if(file.exists(paste0(dirs$spatial_data, "parcels_SPATIAL1.TXT"))){}else{
source(paste0(dirs$code, "0_build_pin_geography.R"))
}
# Create modeling data
source(paste0(dirs$code, "0_build_200class_modeldata.R"))

# Create assessment data
source(paste0(dirs$code, "0_build_200class_assmtdata.R"))

# PLAY IN THE SANDBOX 1_sf_modeling_sandbox.R

# Run automated modeling program
source(paste0(dirs$code, "2_sf_modeling.R"))

# Final valuations
source(paste0(dirs$code, "3_sf_valuation.R"))

# Push outliers to desktop review
source(paste0(dirs$code, "4_sf_desktop_review.R"))

# Export and report on values
source(paste0(dirs$code, "5_sf_export&visualize.R"))

# Compare CAMA and legacy valuations
#source(paste0(dirs$code, "6_legacy_comparison.R"))

# Recover desk review
source(paste0(dirs$code, "7_recover_desktop_reviews.R"))
