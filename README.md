# Cook County Residential CAMA System

This repository houses the code for the Computer Assisted Mass Appraisal system used to assess residential properties. The current assessment calendar divides the county into thirds, with each third being reassessed once every three years. 

## Steps for contributors
### Accounts to create
- [gitlab](https://gitlab.com/)
- [bitbucket](https://bitbucket.org/)

### Progams to install
- [Sourcetree](https://product-downloads.atlassian.com/software/sourcetree/windows/ga/SourceTreeSetup-3.0.12.exe)
- [R](https://cran.r-project.org/bin/windows/base/)
- [R Studio](https://download1.rstudio.org/RStudio-1.1.463.exe)
- [Java](https://javadl.oracle.com/webapps/download/AutoDL?BundleId=236885_42970487e3af4f5aa5bca3f542482c60)
- [MikeTex](https://miktex.org/download/ctan/systems/win32/miktex/setup/windows-x64/basic-miktex-2.9.6942-x64.exe)
- [TexStudio](https://github.com/texstudio-org/texstudio/releases/download/2.12.14/texstudio-2.12.14-win-qt5.exe)

### Git
1. set up accounts
2. install programs
   * link your bitbucket account to sourcetree during sourcetree installation
   * when sourcetree asks you to install mercucial and git, select yes for both
3. create a .ssh folder
   * One way to do this is to open the Start Menu and type `cmd` in the search box. Click on `cmd.exe` when it comes up in the search results. This will open up a Windows command prompt. Enter `mkdir .ssh` in the command prompt. This will create a folder called `.ssh` in your home directory.
4. open sourcetree
   * go to "tools"
   * select "create or import SSH keys"
   * select "generate"
   * save the public and private keys in your .ssh folder (add a password to your private key if you desire)
   * keep the sourcetree window with your newly generated ssh key up and copy it: **pasting this version of your ssh key into putty, gitlab, and bitbucket will help avoid errors**
5. right click the putty icon in the system tray (bottom right portion of task bar)
   * click "add key"
   * locate your key in your .ssh folder and add it
6. add your ssh keys to gitlab and bitbucket
   * in gitlab, go to settings under your profile icon in the top-right corner
     * on the left-hand side of the settings window, scroll down to `SSH Keys` and paste in your public key. title it whatever you like.
   * in bitbucket go to settings under your profile icon in the bottom-left corner
     * under `security` click `SSH Keys` then `Add Key`
7. in sourcetree, select "clone"
   * from gitlab, view the repository you'd like to clone
   * on the left hand side select "projects" and "details"
   * on the right-hand select the blue "clone" button and hit the chain button under "Clone with SSH"
   * paste this into the top line of the "clone" page in sourcetree, and click out of the box
   * if source tree encoutners an error, restart it and repaste the text into the "clone" field
   * under "Advanced Options", select the `development` branch
   * press the "clone" button at the bottom of the page
8. interns: return to the main screen and click the "branch" button near the top
   * create a new branch named `working`
   * when pushing code updates, push to the `development` "remote branch" from the `working` "local branch" (this step can be... confusing, please feel free to ask Rob or Billy to clarify)

### R
* before you start working in RStudio, open it and run `trace(utils:::unpackPkgZip, edit=TRUE)`
  * on line 142 of the file that opens, change `Sys.sleep(0.5)` to `Sys.sleep(2.5)` (this should no longer be an issue in R versions > 3.5)
* if at any point R cannont contact CRAN mirrors to download packages, run `options(download.file.method="libcurl")`
* if at any point R returns the errors `LAPACK routines cannot be loaded` or `maximal number of DLLs reached`, run `file.edit('~/.Renviron')`, enter `R_MAX_NUM_DLLS=1000` in the viewer, save, and reboot RStudio

### ODBC

If you are a contributor with access to the CCAO's internal SQL server, follow these steps.

1. open "ODBC Administration"
2. select the "System DSN" tab
3. click "Add"
4. select "SQL server"
   * Name: CCAODATA
   * Description: ccao sand box
   * Server: 10.128.57.154
5. change to verifying with SQL server authentication using a login
   * login ID & password are both "generalUser"
6. keep all other default options as you click next

### API

If you are a contributor that does not have access to the CCAO's internal SQL server, you can use the data published on the [Cook County Open Data Portal](https://datacatalog.cookcountyil.gov/stories/s/p2kt-hk36).

1. [Model Data](https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Modeling-Data/5pge-nu6u) replaces the main SQL query in 0_build_200class_modeldata.R
2. [Assessment Data](https://datacatalog.cookcountyil.gov/Property-Taxation/Cook-County-Assessor-s-Assessment-Data/bcnq-qi2z) replaces the main SQL query in 0_build_200class_assmntdata.R
 
You can substitute an API query for the SQL query and you'll get the same data back as you would were you to query our internal database.



---
:fire: